const express = require("express");

const router = express.Router();

const userControllers = require('../controllers/userControllers');

const {

	registerUser,
	loginUser,
	setAsAdmin,
	viewOrders,
	viewAllOrders,
	viewUserOrders,
	checkout,
	viewUser

} = userControllers;

const auth = require('../auth');

const {verify,verifyAdmin} = auth;

//register user
router.post('/register',registerUser);

//login user
router.post('/login',loginUser);

//set as admin
router.put('/setAdmin/:id',verify,verifyAdmin,setAsAdmin)

//view user orders
router.get('/orders',verify,viewOrders)

//view all orders
router.get('/allOrders',verify,verifyAdmin,viewAllOrders)

//view all orders
router.get('/orders/:id',verify,verifyAdmin,viewUserOrders)

//checkout order
router.post('/checkout',verify,checkout)

//view user
router.get('/viewUser',verify,viewUser)


module.exports = router;



