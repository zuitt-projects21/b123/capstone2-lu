const express = require("express");

const router = express.Router();

const productControllers = require('../controllers/productControllers');

const {

	getActiveProducts,
	getSingleProduct,
	getAllProducts,
	getCategory,
	createProduct,
	updateProduct,
	archiveProduct,
	activateProduct
	
} = productControllers;

const auth = require('../auth');

const {verify,verifyAdmin} = auth;

//get all active products
router.get('/',getActiveProducts);

//get single product
router.get('/:id',getSingleProduct);

//get all products
router.get('/view/all',verify,verifyAdmin,getAllProducts);

//get active products by category
router.get('/category/:category',getCategory);


//create product
router.post('/create',verify,verifyAdmin,createProduct);

//update product
router.put('/update/:id',verify,verifyAdmin,updateProduct)

//archive product
router.put('/archive/:id',verify,verifyAdmin,archiveProduct)

//activate product
router.put('/activate/:id',verify,verifyAdmin,activateProduct)

module.exports = router;