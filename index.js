const express = require('express');
const mongoose = require("mongoose");
const cors = require("cors");

const app = express();

const port = process.env.PORT || 4000;

mongoose.connect("mongodb+srv://kimlu10239:Victory11@cluster0.qvgky.mongodb.net/eCommerceAPI?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true	
	}
)

//check db connection
let db = mongoose.connection;
db.on("error",console.error.bind(console,"Connection Error."));
db.once("open", ()=>console.log("connected to MongoDB"));

app.use(cors())
//allows a front-end to connect with your back end


app.use(express.json());

//middleware
const userRoutes = require('./routes/userRoutes');
app.use('/users',userRoutes);

const productRoutes = require('./routes/productRoutes');
app.use('/products',productRoutes);

app.listen(port, () => console.log(`Server is running at port ${port}`));	