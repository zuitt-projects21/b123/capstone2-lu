const User = require('../models/User');
const Product = require('../models/Product');

module.exports.getActiveProducts = (req,res) => {

	Product.find({isActive: true})
		.then(result => res.send(result))
		.catch(err => res.send(err))

}

module.exports.getCategory = (req,res) => {
	console.log(req.params);
	Product.find({category: req.params.category})
		.then(result => res.send(result))
		.catch(err => res.send(err))

}

module.exports.getAllProducts = (req,res) => {

	Product.find({})
		.then(result => res.send(result))
		.catch(err => res.send(err))

}

module.exports.getSingleProduct = (req,res) => {
	console.log(req.params.id)
	Product.findById(req.params.id)
		.then(result => res.send(result))
		.catch(err => res.send(err))

}

module.exports.createProduct = (req,res) => {

	let newProduct = new Product({
			name: req.body.name,
			description: req.body.description,
			price: req.body.price,
			category: req.body.category,
			stock: req.body.stock
	})

	newProduct.save()
		.then(result => {
			console.log(result)
			res.send(result)
		})
		.catch(err => res.send(err))

}

module.exports.updateProduct = (req,res) => {

	let updates = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price,
		category: req.body.category,
		stock: req.body.stock
	}

	Product.findByIdAndUpdate(req.params.id,updates,{new:true})
		.then(result => res.send(result)) 
		.catch(err => res.send(err))

}
module.exports.archiveProduct = (req,res) => {

	let updates = {
		isActive: false
	}

	Product.findByIdAndUpdate(req.params.id,updates,{new:true})
		.then(result => res.send({message: `${result.name} has been archived`})) 
		.catch(err => res.send(err))

}

module.exports.activateProduct = (req,res) => {

	let updates = {
		isActive: true
	}

	Product.findByIdAndUpdate(req.params.id,updates,{new:true})
		.then(result => res.send({message: `${result.name} has been activated`})) 
		.catch(err => res.send(err))

}