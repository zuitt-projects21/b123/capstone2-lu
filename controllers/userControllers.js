const User = require('../models/User');
const Product = require('../models/Product');

const bcrypt = require('bcrypt');

//auth
const auth = require('../auth');
const {createAccessToken} = auth;

//Register User
module.exports.registerUser = (req,res) => {

	console.log(req.body);

	if(req.body.password.length < 8) return res.send({message: "Password is too short"});

	const hashedPW = bcrypt.hashSync(req.body.password,10);

	User.findOne({email: req.body.email})
		.then( result => {
			if(result !== null && result.email === req.body.email){

				return res.send("Email already registered. Please use another email to register.")

			} else{

				let newUser = new User({
					
					firstName: req.body.firstName,
					lastName: req.body.lastName,
					mobileNo: req.body.mobileNo,
					email: req.body.email,
					address: req.body.address,
					password: hashedPW,

				})

				newUser.save()
					.then(result => {
						console.log(result);
						 res.send({message: "Registered Successfully"})
					})
					.catch(err => res.send({message: err}))					
			}	
		})

		.catch(err => res.send(err))
		
}

module.exports.loginUser = (req,res) => {

User.findOne({email: req.body.email})
	.then(result => {
		
		if(result === null){
			res.send({message: "User not found"});
		}else {
			const isPasswordCorrect = bcrypt.compareSync(req.body.password,result.password);

			// console.log(isPasswordCorrect);

			if(isPasswordCorrect){

				return res.send({accessToken: createAccessToken(result)});

			}else{

				return res.send({message: "Password incorrect"})
			}
		}
		
	})	

	.catch(err => res.send(err))

}

module.exports.viewUser = (req,res) => {

	//logged in user's details after decoding with auth module's verify()
	console.log(req.user)

	//using req.user.id would mean that you wont need to enter eh database id in the endpoint anymore. will instead show the details of the logged in user with the same endpoint	
	User.findById(req.user.id)
		.then(result => res.send(result))
		.catch(err => res.send(err))
}

module.exports.setAsAdmin = (req,res) => {

	let updates = {
		isAdmin: true
	}

	User.findByIdAndUpdate(req.params.id,updates,{new:true})
		.then(result => res.send({message: `${result.email} set as admin`})) 
		.catch(err => res.send(err))

}

module.exports.viewOrders = (req,res) => {

	User.findById(req.user.id)
		.then(result => res.send(result.orders))
		.catch(err => res.send(err))

}

module.exports.viewUserOrders = (req,res) => {

	User.findById(req.params.id)
		.then(result => res.send(result.orders))
		.catch(err => res.send(err))

}

module.exports.viewAllOrders = (req,res) => {

	User.find({}, {_id:1,firstName:1,lastName:1,mobileNo:1,email:1,orders:1,isAdmin:1})
		.then(result => res.send(result))
		.catch(err => res.send(err))

}


//alt code for view all:

/*module.exports.viewAllOrders = (req,res) => {

	let allOrders = [];

	User.find({})
		.then(result => {
			allOrders = result.map(user => user.orders);
			res.send(allOrders);
		})
		.catch(err => res.send(err))
	

}*/

module.exports.checkout = async (req,res) =>{

	//check if admin first. checkout only for regular registered users
	if (req.user.isAdmin){
		return res.send({
			auth: "Failed",
			message: "Action Forbidden"
		})
	} else{
		//save id for access later:
		let user_id = req.user.id;

		let isUserUpdated = await User.findById(req.user.id)
			.then(user => {
				//checks user orders
				console.log(user.orders);

				//add to user's orders array:
				user.orders.push(req.body)

				return user.save()
				.then(user => {
					/*console.log(user.orders[user.orders.length-1].id);*/
					return user.orders[user.orders.length-1].id 
				}) //assigns order id to isUserUpdated
				.catch(err => false) 
			})

			console.log(isUserUpdated);
			//end the request/response when isUserUpdated returns false.
			if(isUserUpdated === false) return res.send(isUserUpdated);

			//isUserUpdated = latest order id
			//loop to update all products.orders - add 
			
			console.log(req.body);

			let updateCond = 0;
			let unsuccessfulOrder = [];

			for(i=0; i<(req.body.products.length); i++){

				console.log(req.body.products[i].productId);
				let qty = req.body.products[i].quantity;

				let isProductUpdated = await Product.findById(req.body.products[i].productId)
				.then(product => {
					if(product.isActive === true && product.stock >= qty){

						product.orders.push({orderId: isUserUpdated, quantity: qty})
						product.stock -= qty;
						console.log(`stocks left: ${product.stock}`);

						return product.save()
							.then(result => true)
							.catch(err => false)

					} else{
						unsuccessfulOrder.push(product.id);
						return false;
					}
				})
				.catch(err => err)

				console.log(isProductUpdated)
				/*if(isProductUpdated !== true) return res.send(isProductUpdated)*/	
				if(isProductUpdated === true) {updateCond++};	
			}

			console.log(updateCond);
			console.log(req.body.products.length);

			if (updateCond == req.body.products.length){
				return res.send("All products ordered successfully") 	
			} else if(updateCond == 0){
				/*console.log(isUserUpdated)*/

				User.findById(user_id)
					.then(result => {
						let orderNo = result.orders.length - 1; 
						result.orders[orderNo].status = "On Hold";
						/*console.log(result.orders[orderNo].status)*/

						return result.save()
							.then(result => res.send("Order put on hold"))
							.catch(err => res.send(err)) 
					})
					.catch(err => res.send(err))

			}else {
				return res.send({
					message: `${updateCond} out of ${req.body.products.length} products ordered successfully.`,
					unsucessful: unsuccessfulOrder
				})
			}

			

/*			req.body.products.every(product => {

				console.log(product.productId);
				let qty = product.quantity;

				let isProductUpdated = Product.findById(product.productId)
				.then(product => {
					product.orders.push({orderId: isUserUpdated, quantity: qty})

					return product.save()
					.then(product => res.send("Item ordered successfully"))
					.catch(err => res.send(err))

				})
			.then(result => res.send("Items ordered successfully"))
			.catch(err => res.send(err))
				

				console.log(isProductUpdated);
				if(isProductUpdated !== true) return res.send(isProductUpdated);
				if(isUserUpdated !== false && isProductUpdated) return res.send("Item Ordered Successfully.");
			})*/
		
	}
}





