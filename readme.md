Project Name: Lozodoh E-commerce API

//to guide future users
//if request has not body. just add no body.

Features:
User Reg
User Auth
Set User as admin
retrieve all active prod
create prod(admin)
update prod infor(admin)
archive prod(admin
non-admin user checkout(create order)
retrieve authenticated user's orders
retrieve all orders (admin only)

Routes and Request Body:

Registration:
	POST: http://localhost:4000/users/register

	Body: (JSON)
	{
		"firstName": "string",
		"lastName": "string",
		"email": "string",
		"password": "string & at least 8 characters long",
		"mobileNo": "string"
	}

Login:
	POST: http://localhost:4000/users/login

	Body: (JSON)
	{
		"email": "string",
		"password": "string"
	}

Set as admin:
	PUT: http://localhost:4000/users/setadmin/:id

	Body: <no body needed>
	Token: Admin needed 	

Retrieve all Active Products:
	GET: http://localhost:4000/products/

	Body: <no body needed>

Retrieve Single Product:
	GET: http://localhost:4000/products/:id

	Body: <no body needed>	

Create Products:
	POST: http://localhost:4000/products/create

	Body: (JSON)
	{
		"name": "string",
		"description": "string",
		"price": "number",
		"stock": "number"
	}

	Token: Admin needed 

Update Product:
	PUT: http://localhost:4000/products/update/:id
	Body: (JSON)
	{
		"description": "string",
		"price": "number",
		"stock": "number"	
	}

	Token: Admin needed

Archive Products
	PUT: http://localhost:4000/products/archive/:id

	Body: <no body needed>	
	Token: Admin needed


Activate Products
	PUT: http://localhost:4000/products/activate/:id

	Body: <no body needed>	
	Token: Admin needed

Checkout order:
	POST: http://localhost:4000/users/checkout

	Body: (JSON)
	{				

		"totalAmount" : "number",
		"products" : [

			{
				productId: "string",
				productName: "string",
				quantity: "number",
				price: "number"	
			}

		]	
	}