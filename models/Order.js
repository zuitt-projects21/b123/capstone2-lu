//DO NOT USE:

const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({

	datePurchased: {
		type: Date
		default: new Date() 
	},
	
	userId: {
		type: String,
		required: [true, "UserId is required"]
	},
	
	totalAmount: {
		type: Number,
		required: [true, "Order total is required"]
	},

	status: {
		type: String,
		default: "Pending"
	},

	products: [
		{
			productId: {
				type: String,
				required: [true, "Product Id is required."]
			},
			productName:{
				ype: String,
				required: [true, "Product Name is required."]
			},
			quantity: {
				type: Number,
				required: [true, "Quantity is required."]
			}

		}
	]
		
});

module.exports = mongoose.model("Order", orderSchema);