const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({

	name: {
		type: String,
		required: [true, "Product name is required"]
	},

	description: {
		type: String,
		required: [true, "description is required"]
	},

	price: {
		type: Number,
		required: [true, "Item price is required"]
	},

	stock: {
		type: Number,
		required: [true, "Number of stocks is required."]
	},

	category: {
		type: String
	},

	isActive: {
		type: Boolean,
		default: true
	},

	createdOn: {
		type: Date,
		default: new Date() 
	},

	orders: [
		{
			orderId: {
				type: String,
				required: [true, "Order Id is required."]
			},
			quantity: {
				type: Number,
				required: [true, "Order quantity is required."]
			}
		}
	]
		
});

module.exports = mongoose.model("Product", productSchema);