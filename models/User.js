const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({

		firstName: {
			type: String,
			required: [true, "First name is required"]
		},
		lastName: {
			type: String,
			required: [true, "Last name is required"]
		},
		mobileNo: {
			type: String,
			required: [true, "Mobile number is required"]
		},
		email: {
			type: String,
			required: [true, "Email address is required"]
		},		
		password: {
			type: String,
			required: [true, "Password is required"]
		},
		address:{
			type: String,
			required: [true, "Address is required"]
		},
		
		
		isAdmin: {
			type: Boolean,
			default: false
		},
		orders: [
			{				
				datePurchased: {
					type: Date,
					default: new Date()
				},
				totalAmount:{
					type: Number,
					required: [true, "Total amount is required."]
				},
				status: {
					type: String,
					default: "Pending"
				},
				products:[

					{
						productId:{
							type: String,
							required: [true, "Product Id is required."]
						},
						productName:{
							type: String,
							required: [true, "Product Name is required."]
						},
						quantity:{
							type: Number,
							required: [true, "Quantity is required."]
						},
						price:{
							type: Number,
							required: [true, "Price is required."]
						}	
					}

				]	
			}
		]
		
	});

module.exports = mongoose.model("User", userSchema);